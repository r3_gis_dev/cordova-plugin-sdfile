package com.phonegap.plugin.SDFilePlugin;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.*;

import org.apache.cordova.*;

import android.util.Log;

public class SDFilePlugin extends CordovaPlugin {
	@Override
	public boolean execute(String action, JSONArray args, CallbackContext cbc) {
		Log.v("SDFilePlugin.execute", "SDFilePlugin.execute, action: "+action);
		try {
			if (action.equals("copy")) {
				JSONObject o = args.getJSONObject(0);
				String source = o.getString("source");
				String destination = o.getString("destination");
				Log.v("SDFilePlugin.execute", "copy "+source+" to "+destination);
				this.copy(source, destination);
			} else if (action.equals("move")) {
				JSONObject o = args.getJSONObject(0);
				String source = o.getString("source");
				String destination = o.getString("destination");
				Log.v("SDFilePlugin.execute", "move "+source+" to "+destination);
				this.move(source, destination);
			}

		} catch (JSONException e) {

			// TODO: signal JSON problem to JS
			Log.v("SDFilePlugin JSONException", e.getMessage());
			return false;
		} catch (IOException e) {
			Log.v("SDFilePlugin IOException", e.getMessage());
        	return false;
        }

		cbc.success();
		return true;
	}
	
	void copy(String source, String destination) throws IOException {
		InputStream in = new FileInputStream(source);
		OutputStream out = new FileOutputStream(destination);
		
    	// Transfer bytes from in to out
    	byte[] buf = new byte[1024];
    	int len; while ((len = in.read(buf)) > 0) out.write(buf, 0, len);
    	in.close(); out.close();
    
    }
    
	void move(String source, String destination) throws IOException {
		this.copy(source, destination);
		File oldFile = new File(source);
		oldFile.delete();
	}
}


